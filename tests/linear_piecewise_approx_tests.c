/*
 * linear_piecewise_approx_tests.c
 */


#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>
#include <math.h>
#include "./../src/linear_piecewise_approx.h"


void mocked_limiter(hydro_t *ql, hydro_t *q, hydro_t *qr, hydro_t *dq) {
  for (int i = rho_h; i <= p_h; i++)
    dq->vars[i] = 2.0;
  mock(ql, q, qr, dq);
}


Describe(linear_piecewise_approx);
BeforeEach(linear_piecewise_approx) {};
AfterEach(linear_piecewise_approx) {};


Ensure(linear_piecewise_approx, )
{
  hydro_t q1 = {.vars = {1.2, 2.3, 3.4, 4.5, 5.6}};
  hydro_t q2 = {.vars = {2.3, 3.4, 4.5, 5.6, 6.7}};
  hydro_t ql; /* Left side of the interface */
  hydro_t qr; /* Right side of the interface */
  hydro_t q3 = {.vars = {3.4, 4.5, 5.6, 6.7, 7.8}};
  hydro_t q4 = {.vars = {4.5, 5.6, 6.7, 7.8, 8.9}};

  hydro_t *hydro_arr[4] = {&q1, &q2, &q3, &q4};

  expect(mocked_limiter, will_return(2));
  expect(mocked_limiter, will_return(2));

  linear_piecewise_approx(hydro_arr, mocked_limiter, &ql, &qr);

  for (int hvar = rho_h; hvar <= p_h; hvar++)
  {
    assert_that_double(ql.vars[hvar], is_equal_to_double(q2.vars[hvar] + 1.0));
    assert_that_double(qr.vars[hvar], is_equal_to_double(q3.vars[hvar] - 1.0));
  }
}
