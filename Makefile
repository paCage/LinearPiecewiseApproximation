MODULE_NAME := LinearPiecewiseApproximation

MAJOR := 0
MINOR := 0
PATCH := 0


OBJS := linear_piecewise_approx.o


TEST_OBJS := linear_piecewise_approx_tests.o


LIBS :=
paCages := HydroBase


include ./Makefile.paCage/Makefile
