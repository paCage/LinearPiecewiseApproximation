# Linear Piecewise Approximation
Extrapolating slope limited state variables to the left and right sides of
their interfaces.

## Description
MUSCL[^1] based numerical schemes are usually using a linear piecewise
approximation to each cell by using the left and the right slope limited
extrapolated states. This results in the following high resolution, TVD
discretisation scheme [Wikipedia],

```math
\frac{\text{d} u_i}{\text{d} t} + \frac{1}{\Delta x_i} \left[
  F \left( u_{i + \frac{1}{2}}^* \right) - F \left( u_{i - \frac{1}{2}}^* \right)
\right] = 0
```

where $`u`$ represents a state variable and $`F`$ a flux variable.

The numerical fluxes corresponds to a nonlinear combination of first- and
second-order approximation to the continuous flux function.

$`u_{i + \frac{1}{2}}^*`$ and $`u_{i - \frac{1}{2}}^*`$ represent scheme dependent
functions of the limited extrapolated cell edge variables, i.e.,

```math
u_{i \pm \frac{1}{2}}^* = u_{i \pm \frac{1}{2}}^*\left(
  u_{i \pm \frac{1}{2}}^L, u_{i \pm \frac{1}{2}}^R   
\right)
```

where,

```math
\begin{aligned}
u_{i + \frac{1}{2}}^L &= u_i     + \frac{1}{2} \phi(r_i)     \left( u_i     - u_{i-1} \right) \\
u_{i + \frac{1}{2}}^R &= u_{i+1} - \frac{1}{2} \phi(r_{i+1}) \left( u_{i+2} - u_{i+1} \right) \\
u_{i - \frac{1}{2}}^L &= u_{i-1} + \frac{1}{2} \phi(r_{i-1}) \left( u_{i-1} - u_{i-2} \right) \\
u_{i - \frac{1}{2}}^R &= u_i     - \frac{1}{2} \phi(r_i)     \left( u_{i+1} - u_i     \right) \\
\end{aligned}
```

The $`\phi(r)`$ is a limiter function which limits the slope of the piecewise
approximation to ensure the solution is TVD.

The goal here is to calculate $`u_{i \pm \frac{1}{2}}^{L,R}`$ based on a given
slope limiter.

[^1]: Monotonic Upwind Scheme for Conservation Laws (van Leer, 1979),  
