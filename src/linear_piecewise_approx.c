/*
 * linear_piecewise_approx.c
 * tests_file: linear_piecewise_approx_tests.c
 *
 * Extrapolating state variables to their middle edge and applying a given
 * slope limiter at the same time
 *
 * | q(-2) | q(-1) L->|<-R q(1) |  q(2)  |
 *                    ^         ^
 *                  edge
 *
 * @param: q: An array of 4 hydro states (look above)
 * @param: limiter: Pointer to a limiter function
 * @param: ql: Constructed left hydro state
 * @param: qr: Constructed right hydro state
 *
 * @return: void
 */


#include <HydroBase.h>
#include "./linear_piecewise_approx.h"

void linear_piecewise_approx(hydro_t **q,
    void limiter(hydro_t*, hydro_t*, hydro_t*, hydro_t*),
    hydro_t *ql, hydro_t *qr)
{
  hydro_t dq_L, dq_R;

  limiter(q[0], q[1], q[2], &dq_L);
  limiter(q[1], q[2], q[3], &dq_R);

  for (int hvar = rho_h; hvar <= p_h; hvar++)
  {
    ql->vars[hvar] = q[1]->vars[hvar] + 0.5 * dq_L.vars[hvar];
    qr->vars[hvar] = q[2]->vars[hvar] - 0.5 * dq_R.vars[hvar];
  }
}
