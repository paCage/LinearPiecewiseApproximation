#ifndef _LINEAR_PIECEWISE_APPROX_H_
#define _LINEAR_PIECEWISE_APPROX_H_


#include <HydroBase.h>


#ifdef __cplusplus
extern "C" {
#endif

void linear_piecewise_approx(hydro_t **q,
    void limiter(hydro_t*, hydro_t*, hydro_t*, hydro_t*),
    hydro_t *ql, hydro_t *qr);

#ifdef __cplusplus
}
#endif


#endif /* _LINEAR_PIECEWISE_APPROX_H_ */
